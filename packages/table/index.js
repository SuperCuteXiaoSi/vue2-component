import Table from './src'

Table.install = function(Vue) {
    Vue.component(Table.name, Table)
}

export default Table;