import Table from './table'
import * as wxapp from './wxapp'


const components = [
  Table
]

const install = function(Vue, opts = {}) {
  components.forEach(component => {
    console.log(component)
      Vue.component(component.name, component)
  });
  Vue.prototype.$wx = wxapp
}

export default {
  install
}