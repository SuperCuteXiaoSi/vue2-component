// #ifdef H5
import jweixin from 'jweixin-module'
// #endif
// import { request } from '@/utils/http'
// import { WECHAT_HOST, WECHAT_SHARE_ORIGIN } from '@/config/env'

const API_LIST = ['updateAppMessageShareData', 'updateTimelineShareData', 'openLocation', 'getLocation']
const SHARE_TITLE = '健康知识问答赢奖品'
const SHARE_DESC = '还可查询周边AED'
const SHARE_LINK = window.location.href.split('#')[0]
const SHARE_ICON = 'http://api.aed.jizhit.com/img/fenxiang.jpg'//图片

const init = (success, fail) => {
  const { origin, pathname, search } = window.location
  request({
    method: 'GET',
    url: '/get/jssdk',
    data: { url: window.location.href.split('#')[0]},
    success: (data) => {
      console.log('data', data)
      const { appid, timestamp, noncestr, signature } = data.data.data
      console.log(appid, timestamp, noncestr, signature)
      jweixin.config({
        debug: process.env.NODE_ENV === 'development' ? true : false,
        // debug:true,
        appId: appid,
        timestamp,
        nonceStr: noncestr,
        signature,
        jsApiList: API_LIST
      })
      jweixin.ready(() => {
        success && success()
      })
      jweixin.error((err) => {
        console.log('验证签名无效？',err.err_msg)
        fail && fail({ msg: err.err_msg })
      })
    },
    fail: (err) => {
      fail && fail(err)
    }
  })
}

const ready = jweixin.ready
const error = jweixin.error
const openLocation = jweixin.openLocation
const getLocation = jweixin.getLocation

const share = (options) => {
  options = options || {
    title: SHARE_TITLE,
    desc: SHARE_DESC,
    link: SHARE_LINK,
    img: SHARE_ICON,
    success: () => { }
  }
  jweixin.ready(() => {
    jweixin.updateAppMessageShareData({
      title: options.title,
      desc: options.desc,
      link: options.link,
      imgUrl: options.img,
      success: options.success
    })
    jweixin.updateTimelineShareData({
      title: options.title,
      link: options.link,
      imgUrl: options.img,
      success: options.success
    })
  })
}


const getQueryVariable = (variable) => {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) { return pair[1]; }
  }
  return (false);
}

export default {
  init,
  ready,
  error,
  openLocation,
  getLocation,
  share,
  getQueryVariable
}