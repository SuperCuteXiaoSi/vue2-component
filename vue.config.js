const path = require('path')

function resolve(dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    pages: {
        index: {
            entry: 'src/main.js',
            template: 'public/index.html',
            filename: 'index.html'
        }
    },
    configureWebpack: {
        // provide the app's title in webpack's name field, so that
        // it can be accessed in index.html to inject the correct title.
        resolve: {
            alias: {
                '@': resolve('src'),
                'source': resolve('source'),
            }
        },
        performance: {
            hints:'warning',
            //入口起点的最大体积 整数类型（以字节为单位）
            maxEntrypointSize: 50000000,
            //生成文件的最大体积 整数类型（以字节为单位 300k）
            maxAssetSize: 30000000,
            //只给出 js 文件的性能提示
            assetFilter: function(assetFilename) {
                return assetFilename.endsWith('.js');
            }
        },
        // externals: /mixin.scss/i,
    },
    chainWebpack(config) {
        // const oneOfsMap = config.module.rule('scss').oneOfs.store
        // oneOfsMap.forEach(item => {
        //     item
        //         .use('sass-resources-loader')
        //         .loader('sass-resources-loader')
        //         .options({
        //             // Provide path to the file with resources
        //             // 要公用的scss的路径
        //             resources: './source/styles/mixin.scss'
        //         })
        //         .end()
        // })
        // 把图片打包进js里
        config.module
            .rule('images')
            .use('url-loader')
            .loader('url-loader')
            .tap(options => Object.assign(options, { limit: 300000 }))
    }
}